#!/usr/bin/env bash

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)

ci_utils_script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)

PROJECT_ROOT=$(realpath "${ci_utils_script_dir}/..")

QTR_LOG_LEVELS=${QTR_LOG_LEVELS:-'DEBUG INFO WARNING ERROR'}

set -eo pipefail


log() {
    local severity=$1
    shift

    local ts=$(date "+%Y-%m-%d %H:%M:%S%z")

    # See https://stackoverflow.com/a/46564084
    if [[ ! " ${QTR_LOG_LEVELS} " =~ .*\ ${severity}\ .* ]] ; then
        log ERROR "Unexpected severity '${severity}', must be one of: ${QTR_LOG_LEVELS}"
        severity=ERROR
    fi

    # See https://stackoverflow.com/a/29040711 and https://unix.stackexchange.com/a/134219
    local module=$(caller | awk '
        function basename(file, a, n) {
            n = split(file, a, "/")
            return a[n]
        }
        { printf("%s:%s\n", basename($2), $1) }')

    case "${severity}" in
        ERROR)
            color_start='\033[0;31m' # Red
            ;;
        WARNING)
            color_start='\033[1;33m' # Yellow
            ;;
        INFO)
            color_start='\033[1;32m' # Light Green
            ;;
        DEBUG)
            color_start='\033[0;34m' # Blue
            ;;
    esac
    color_end='\033[0m'

    printf "${ts} ${color_start}${severity}${color_end} [${module}]: ${color_start}$*${color_end}\n" >&2
}

total_missed_instructions=0
total_covered_instructions=0
total_missed_lines=0
total_covered_lines=0
total_missed_complexity=0
total_covered_complexity=0
total_missed_methods=0
total_covered_methods=0

for coverage_csv in "${PROJECT_ROOT}"/*/target/site/jacoco/jacoco.csv
do
    log INFO "Processing '${coverage_csv}' JaCoCo coverage CSV report file"

    if [[ "$(uname -o)" == 'Msys' ]] ; then
        dos2unix "${coverage_csv}"
    fi

    while IFS=, read -r group package class missed_instructions covered_instructions \
                        missed_branches covered_branches missed_lines covered_lines \
                        missed_complexity covered_complexity missed_methods covered_methods
    do
        # Skipping first CSV header line
        if [[ "${group}" == 'GROUP' ]] ; then
            continue
        fi

        total_missed_instructions=$((${total_missed_instructions} + ${missed_instructions}))
        total_covered_instructions=$((${total_covered_instructions} + ${covered_instructions}))

        total_missed_lines=$((${total_missed_lines} + ${missed_lines}))
        total_covered_lines=$((${total_covered_lines} + ${covered_lines}))

        total_missed_complexity=$((${total_missed_complexity} + ${missed_complexity}))
        total_covered_complexity=$((${total_covered_complexity} + ${covered_complexity}))

        total_missed_methods=$((${total_missed_methods} + ${missed_methods}))
        total_covered_methods=$((${total_covered_methods} + ${covered_methods}))
    done < "${coverage_csv}"
done

total_instructions=$((${total_covered_instructions} + ${total_missed_instructions}))
log INFO "Covered instructions: ${total_covered_instructions}, missed instructions: ${total_missed_instructions}, total instructions: ${total_instructions}"
instructions_coverage=$((${total_covered_instructions} * 100 / ${total_instructions}))

total_lines=$((${total_covered_lines} + ${total_missed_lines}))
log INFO "Covered lines: ${total_covered_lines}, missed lines: ${total_missed_lines}, total lines: ${total_lines}"
lines_coverage=$((${total_covered_lines} * 100 / ${total_lines}))

total_complexity=$((${total_covered_complexity} + ${total_missed_complexity}))
log INFO "Covered complexity: ${total_covered_complexity}, missed complexity: ${total_missed_complexity}, total complexity: ${total_complexity}"
complexity_coverage=$((${total_covered_complexity} * 100 / ${total_complexity}))

total_methods=$((${total_covered_methods} + ${total_missed_methods}))
log INFO "Covered methods: ${total_covered_methods}, missed methods: ${total_missed_methods}, total methods: ${total_methods}"
methods_coverage=$((${total_covered_methods} * 100 / ${total_methods}))

echo "JaCoCo: Instructions coverage: ${instructions_coverage}%"
echo "JaCoCo: Lines coverage: ${lines_coverage}%"
echo "JaCoCo: Complexity coverage: ${complexity_coverage}%"
echo "JaCoCo: Methods coverage: ${methods_coverage}%"
