package com.tutorial.serviceA;

import java.net.URI;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalManagementPort;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HealthCheckTest {

  @Autowired
  private WebTestClient webTestClient;

  @LocalManagementPort
  private Integer managementPort;

  @Test
  void Should_Return_200_On_HealthCheckHit() {
    this.webTestClient
        .get()
        .uri(URI.create("http://localhost:" + managementPort + "/actuator/health"))
        .exchange()
        .expectStatus()
        .is2xxSuccessful()
        .expectBody()
        .jsonPath("$.status").isEqualTo("UP")
        .jsonPath("$.components.livenessState.status").isEqualTo("UP")
        .jsonPath("$.components.readinessState.status").isEqualTo("UP")
        .jsonPath("$.components.ping.status").isEqualTo("UP");
  }
}
